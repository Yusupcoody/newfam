const Faq = () => {
    return (
        <section className="bg-white dark:bg-gray-900">
    <div className="container px-6 py-12 mx-auto">
        <h1 className="text-2xl font-semibold text-gray-800 lg:text-4xl dark:text-white">Frequently asked questions.</h1>

        <div className="grid grid-cols-1 gap-8 mt-8 lg:mt-16 md:grid-cols-2 xl:grid-cols-3">
            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Apa keunggulan Famitra ?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                     Famitra terus meningkatkan efisiensi dan mengoptimalkan biaya logistik berdasarkan standar operasional yang tinggi, sistem transportasi dan distribusi gudang yang kuat, dan aplikasi logistik yang canggih, untuk meningkatkan nilai distribusi bisnis bagi pelanggan dan menciptakan pengalaman logistik terbaik di Indonesia.
                    </p>
                </div>
            </div>

            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Sistem Pembayaran Famitra ?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                    Untuk pelanggan non-corporate, pembayaran dapat dilakukan secara tunai ketika paket diserahkan ke drop point.Bagi pelanggan corporate, sistem pembayaran dapat dilakukan dengan syarat dan ketentuan yang khusus.
                    </p>
                </div>
            </div>

            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Bagaimana cara melakukan pengecekan resi ?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                    Anda dapat melakukan pengecekan No. Resi Famitra.co.id . </p>
                </div>
            </div>

            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Bagaimana dan kapan Anda mendapatkan Resi ?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                    Nomor resi akan diberikan langsung oleh pihak Famitra pada saat paket dikirimkan melalui drop point.
                    </p>
                </div>
            </div>

            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Apa yang terjadi jika customer mengirimkan paket yang berisikan barang berbahaya (Dangerous Goods)?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                    Bagi customer yang dengan sengaja mengirimkan barang yang dilarang atau barang berbahaya maka berdasarkan Pasal 47 UU No. 38 Th. 2009, customer akan dipidana dengan hukuman penjara paling lama 5 (lima) tahun atau denda paling banyak Rp. 1.000.000.000,- (Satu Miliar Rupiah) dan pihak penyedia jasa pengiriman (Famita) tidak bisa dikenakan pertanggungjawaban atas kejadian tersebut
                    </p>
                </div>
            </div>

            <div>
                <div className="inline-block p-3 text-white bg-blue-600 rounded-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div>
                    <h1 className="text-xl font-semibold text-gray-700 dark:text-white">Bagaimana Cara Mwlakukan Sewa Armada ?</h1>

                    <p className="mt-2 text-sm text-gray-500 dark:text-gray-300 text-left">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident placeat, consequatur eveniet veritatis quos dignissimos beatae dolores exercitationem laboriosam officia magnam atque blanditiis illum doloremque magni ex corrupti tempora quis.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
       
    )
}

export default Faq;