import Image from 'next/image'

const Trackpaket = () => {
    return (
        <div>
            <div data-v-6b960f04="" className="resultSty" style="">
                <div data-v-6b960f04="" className="fl flex item-center">
                    <input data-v-6b960f04="" type="textarea" rows="2" borderless="" placeholder="Lacak nomor waybill, satu kali pencarian maksimal 10 nomor,
silakan gunakan spasi atau enter untuk pemisah" className="input_search page_index_textarea"/>
                        <span data-v-6b960f04="" className="search_btn flex items-center justify-center">
                            <i data-v-6b960f04="" className="iconfont icon-sousuo_chaxunyundan"></i>
                        </span><span data-v-6b960f04="" className="delete_btn" style="display: none;">
                            <i data-v-6b960f04="" className="iconfont icon-icon-qingkong"></i>
                        </span>
                </div>
                <div data-v-6b960f04="" className="no-data">
                    <Image data-v-6b960f04="" src="/img/emptyResult1.f0824a5e.png" alt="" srcset="" style="width: 240px; height: 240px;"/>
                </div>
            </div>
        </div>
    )
}

export default Trackpaket;