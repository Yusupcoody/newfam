const Armada = () => {
    return (
        <>
            <div className="container flex flex-col px-6 py-4 mx-auto space-y-6 lg:h-[32rem] lg:py-16 lg:flex-row lg:items-center">
                <div className="flex flex-col items-center w-full lg:flex-row lg:w-1/2">

                    <div className="max-w-lg lg:mx-12 lg:order-2">
                        <h1 className="text-3xl font-medium tracking-wide text-gray-800 dark:text-white lg:text-4xl">Sewa Armada</h1>
                        <p className="p-2 indent-0.5 text-justify">
                            Kapan pun Anda butuhkan, selalu ada truk dan sistem pengiriman yang berkualitas tersedia untuk Anda. Harga sudah terjamin dan aman, serta kiriman sudah pasti diasuransikan langsung oleh Famitra
                        </p>
                        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Sewa Sekarang
                        </button>
                    </div>
                </div>

                <div className="flex items-center justify-center w-full h-96 lg:w-1/2">
                    <img className="object-cover w-full h-full max-w-2xl rounded-md" src="./img/truck.png" alt="apple watch photo" />
                </div>
            </div>
        </>
    )
}

export default Armada;