import { Button } from 'bootstrap';
import Image from 'next/image'

const CheckResi = () => {
    return (
        <div className="w-full border bg-white">
            <div className="w-full p-6">
                <div className="font-semibold text-base text-slate-900 mb-3">Cek Resi</div>
                <div className="grid grid-cols-2 gap-1">
                    <div class="flex justify-center">
                        <div class="mb-7 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Berat Barang</label>
                            <input type="text"
                                className="form-control block w-full px-3 py-1.5 text-base font-normal  text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    {/*  */}
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                           <button type='button'>Cek</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default CheckResi;

