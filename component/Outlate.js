const Outlate = () => {
    return(
        <div className="w-full border bg-white">
            <div className="w-full p-6">
                <div className="flex">
                    <div class="flex justify-center">
                        <div class="mb-7">
                            <input type="text"
                                className="form-control block px-3 py-1.5 text-base font-normal text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none inputSearch"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                        <button type="button" className="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                                Cek
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Outlate;