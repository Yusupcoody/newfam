import Image from "next/image";

const CekOngkir = () => {
    return (
        <div className="w-full border bg-white">
            <div className="w-full p-6">
                <div className="font-semibold text-base text-slate-900 mb-3">Cek Harga Truck</div>
                <div className="grid grid-cols-3 gap-3">
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Berat Barang</label>
                            <input type="text"
                                className="form-control block w-full px-3 py-1.5 text-base font-normal  text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    {/*  */}
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Volume</label>
                            <input type="text"
                                className="form-control block w-full px-3 py-1.5 text-base font-normal  text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    <div class="flex justify-center">
                        <div className="mb-3 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Metode Pembayaran</label>
                            <select className="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal  text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                <option selected>Pilih Metode pembayaran</option>
                                <option value="1">Cash</option>
                                <option value="2">DFOD</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-3 gap-3">
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Kota Asal</label>
                            <input type="text"
                                className="form-control block w-full px-3 py-1.5 text-base font-normal  text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    {/*  */}
                    <div class="flex justify-center">
                        <div class="mb-3 xl:w-96">
                            <label for="exampleFormControlInput1" className="form-label inline-block mb-2 text-gray-700"
                            >Kota Tujuan</label>
                            <input type="text"
                                className="form-control block w-full px-3 py-1.5 text-base font-normal  text-gray-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-outm-0  focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                id="exampleFormControlInput1"
                                placeholder="Example label"
                            />
                        </div>
                    </div>
                    <div className="flex justify-center mt-8">
                        <div class="mb-3 xl:w-96">
                            <button type="button" className="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                                Cek Harga Truk
                            </button>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default CekOngkir;