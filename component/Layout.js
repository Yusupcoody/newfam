import Footer from "./footer";
import Intro from "./Intro";
import Navbar from "./navbar";

const Layout = ({children}) => {
    return(
        <div>
          <Navbar/>
          <Intro/>
          {children}
          <Footer/>
        </div>
    )
}

export default Layout;