import Image from 'next/image';


const Product = () => {
    return (
        <>
            <section className="bg-white dark:bg-gray-900">
                <div className="container px-6 py-10 mx-auto">
                    <div className="text-center">
                        <h1 className="text-3xl font-semibold text-gray-800 capitalize lg:text-4xl dark:text-white">Layanan Kami</h1>
                    </div>

                    <div className="grid grid-cols-1 gap-8 mt-8 lg:grid-cols-2">
                        <div>
                            <img className="relative z-10 object-cover w-full rounded-md h-96" src="./img/logistic.jpg" alt=""/>

                                <div className="relative z-20 max-w-lg p-6 mx-auto -mt-20 bg-white rounded-md shadow dark:bg-gray-900">
                                    <a href="#" className="font-semibold text-gray-800 hover:underline dark:text-white md:text-xl text-center">
                                        Kargo
                                    </a>
                                </div>
                        </div>

                        <div>
                            <img className="relative z-10 object-cover w-full rounded-md h-96" src="./img/delivery.jpg" alt=""/>

                                <div className="relative z-20 max-w-lg p-6 mx-auto -mt-20 bg-white rounded-md shadow dark:bg-gray-900">
                                    <a href="#" className="font-semibold text-gray-800 hover:underline dark:text-white md:text-xl text-center">
                                        Logistik
                                    </a>
                                </div>
                        </div>
                    </div>
                </div>
            </section>

        </>
    );
}

export default Product;