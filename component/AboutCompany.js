import Image from "next/image";
import styles from "../styles/AboutCompany.module.css"

const AboutCompany  = () => {
    return(
        <div className={styles.addresLink}>
        <div className={styles.addresItem}>
          <p>Kantor Pusat</p>
          <p>Auto Part Pasar Segar - Citra Garden.2
            <br />
            Blok KDA.No.1
            <br />
            Jl. Utan Jati - Peta Timur
            <br />
            Kalideres - Jakarta Barat
            <br />
            Indonesia
          </p>
          <p>
              Contact center. 081-1166-7988
            <br/>
              email . info@famitra.co.id
            <br/>
          </p>
          <p>
            {/* <Link href=""></Link>
            <Link href=""></Link>
            <Link href=""></Link>
            <Link href=""></Link> */}
          </p>
        </div>

      </div>
    )
}

export default AboutCompany;