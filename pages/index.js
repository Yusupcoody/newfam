import Head from 'next/head'
import Image from 'next/image'
import Armada from '../component/Armada'
import Mitra from '../component/Mitra'
import Product from '../component/Product'
import SearchBox from '../component/SearchBox'
import ServiceGurananatee from '../component/ServiceGuarantee'
import styles from '../styles/Home.module.css'

export default function Home() {
  
  return (
    <div className={styles.container}>
      <Head>
        <title>Famitra Express</title>
        <meta name="description" content="Generated by create next app" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <SearchBox/>
      <Armada/>
      <Product/>
      <ServiceGurananatee/>
      <Mitra/>
    </div>
  )
}
