import Head from 'next/head'
// import '../styles/About.css'
import Image from 'next/image'
import Link from 'next/link'
import Faq from '../component/Faq'

const problem = () => {
    return (
        <div>
            <Head>
                <title> Famitra - Informasi</title>
                <meta name="description" content="Informasi" />
                {/* <link rel="icon" href="/favicon.ico" /> */}
            </Head>
            <div className="flex md:flex-row-reverse flex-wrap">
                <div className="w-full md:w-full bg-white p-4 text-center text-gray-700 py-4 px-4">
                  <Faq/>
                </div>
            </div>
        </div>
    )

}

export default problem;