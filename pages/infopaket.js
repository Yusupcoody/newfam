import CekOngkir from "../component/cekOngkir";
import CheckResi from "../component/CheckResi";
import Outlate from "../component/Outlate";
import TruckRental from "../component/TruckRental";
import { withRouter } from "next/router";


const infopaket = ({router}) => {
    const {
        query: { tab }

    } = router

    // logic tab
    const isTabOne = tab === "1" || tab == null 
    const isTabTwo = tab === "2"
    const isTabThree = tab === "3"
    const isTabFour = tab === "4"

    return (
        <div>
            <div className="order-bottom">
                <div className="text-left py-10 mx-16 widthBox">
                    <div className="border-b-2">
                        <div className="inline-flex border-b border-gray-200 dark:border-gray-700">
                            <Link href={{ pathname: "/", query: { tab: "1" } }} className="h-10 px-4 py-2 -mb-px text-sm text-center text-blue-600 bg-transparent border-b-2 border-blue-500 sm:text-base dark:border-blue-400 dark:text-blue-300 whitespace-nowrap focus:outline-none">
                                Lacak Paket
                            </Link>

                            <Link href={{ pathname: "/", query: { tab: "2" } }} className="h-10 px-4 py-2 -mb-px text-sm text-center text-gray-700 bg-transparent border-b-2 border-transparent sm:text-base dark:text-white whitespace-nowrap cursor-base focus:outline-none hover:border-gray-400">
                                Cek cekOngkir
                            </Link>

                            <Link href={{ pathname: "/", query: { tab: "3" } }} className="h-10 px-4 py-2 -mb-px text-sm text-center text-gray-700 bg-transparent border-b-2 border-transparent sm:text-base dark:text-white whitespace-nowrap cursor-base focus:outline-none hover:border-gray-400">
                                Cek Lokasi
                            </Link>

                            <Link href={{ pathname: "/", query: { tab: "4" } }} className="h-10 px-4 py-2 -mb-px text-sm text-center text-gray-700 bg-transparent border-b-2 border-transparent sm:text-base dark:text-white whitespace-nowrap cursor-base focus:outline-none hover:border-gray-400">
                                Sewa Truck
                            </Link>
                        </div>
                        <div className="border bg-white w-full h-4/5">
                            {/* {isTabOne && <CheckResi/>} */}
                            {isTabOne && <CekOngkir/>}
                            {isTabOne && <Outlate/>}
                            {isTabOne && <TruckRental/>}
                           
                        </div>
                    </div>
                </div>
            </div>



        </div>
    )
}

export default infopaket;