import Head from "next/head"
import Image from "next/image"

const joinus = () => {
    return (
        <div className="py-3">
            <Head>
                <title> Famitra - Mitra</title>
                <meta name="description" content="About" />
                {/* <link rel="icon" href="/favicon.ico" /> */}
            </Head>
            <section className="w-full max-w-2xl px-6 py-9 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800">
                <h2 className="text-3xl font-semibold text-center text-gray-800 dark:text-white">Kerjasama Franchise</h2>

                <div className="mt-6 ">
                    <div className="items-center -mx-2 mb-2 md:flex">
                        <div className="w-full mx-2">
                            <label className="block text-sm font-medium text-gray-600 dark:text-gray-200" aria-required>*Nama Anda</label>
                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="text" />
                        </div>

                        <div className="w-full mx-2 mt-4 md:mt-0">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">E-mail</label>

                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border  dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="email" />
                        </div>
                    </div>
                    <div className="items-center -mx-2 mb-2 md:flex">
                        <div className="w-full mx-2">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">No telepone</label>

                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border  dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="text" />
                        </div>

                        <div className="w-full mx-2 mt-4 md:mt-0">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">E-mail</label>

                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="email" />
                        </div>
                    </div>
                    <div className="items-center -mx-2 mb-2 md:flex">
                        <div className="w-full mx-2">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Provinsi </label>
                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border  dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="text" />
                        </div>

                        <div className="w-full mx-2 mt-4 md:mt-0">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Detail Alamat</label>
                            <input className="block w-full px-4 py-2 text-gray-700 bg-white border dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 inputForm" type="email" />
                        </div>
                    </div>
                    <div className="items-center -mx-2 mb-2 md:flex">
                        <div className="w-full mx-2">
                            <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Pengalaman </label>
                            <div class="flex justify-start">
                                <div>
                                    <div className="form-check">
                                        <input className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                        <label className="form-check-label inline-block text-gray-800" for="flexRadioDefault1">
                                            Ya
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                                        <label className="form-check-label inline-block text-gray-800" for="flexRadioDefault2">
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div className="w-full mt-4">
                        <label className="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Message</label>

                        <textarea className="block w-full h-40 px-4 py-2 text-gray-700 bg-white border rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-blue-300 focus:ring-opacity-40"></textarea>
                    </div>

                    <div className="flex justify-center mt-6">
                        <button className="px-4 py-2 text-white transition-colors duration-300 transform bg-blue-700 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-gray-600">Send
                            Message
                        </button>
                    </div>
                </div>
            </section>

        </div>
    )
}

export default joinus;