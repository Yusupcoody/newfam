import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import Mitra from '../component/Mitra'



const about = () => {
    return (
        <div>
            <Head>
                <title> Famitra - About</title>
                <meta name="description" content="About" />
                {/* <link rel="icon" href="/favicon.ico" /> */}
            </Head>
            <section className="bg-white dark:bg-gray-900">
                <div className="container flex flex-col items-center px-4 py-12 mx-auto text-center">
                    <h2 className="text-3xl font-bold tracking-tight text-gray-600 dark:text-white">
                        Visi
                    </h2>

                    <p className="block max-w-2xl mt-4 text-xl text-gray-500 dark:text-gray-300">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iusto recusandae tenetur iste quaerat voluptatibus quibusdam nam repudiandae </p>

                </div>
                <>
                <div className="bg-white dark:bg-gray-800 lg:mx-8 lg:flex  lg:shadow-lg lg:rounded-lg">
                    <div className="lg:w-1/2">
                        <div className="h-64 bg-cover lg:rounded-lg lg:h-full">
                            <img src=''>
                            </img>
                        </div>
                    </div>

                    <div className="max-w-xl px-6 py-12  lg:w-1/2">
                        <h2 className="text-2xl font-bold text-gray-800 dark:text-white md:text-3xl">CEOFOUNDER</h2>
                        <h5>CEO</h5>
                        <h6>Tavip Dwi Komara</h6>
                        <p className="mt-4 text-gray-600 dark:text-gray-400">Tavip Dwi Komara adalah Co-Founder dan CEO di Famitra Technologies.</p>

                        <div className="mt-8">
                            <h5>Directur</h5>
                            <h6>Abdan</h6>
                            <p className="mt-4 text-gray-600 dark:text-gray-400">Abdan adalah Directur dan CEO di Famitra Technologies.</p>

                        </div>
                    </div>
                </div>
                </>
                <Mitra/>
            </section>


        </div>
    )

}

export default about;