import Image from 'next/image'

const register = () => {
    return(
        <div>
            <div className="register_box clear_b">
                <Image src=""/>
                <Image  src="" />
                <form className="register_cont" id="register">
                    <div className="title_reg">
                        <h5>Register</h5>
                    </div>
                    <div className="reg_content">
                        <div className="reg_inp">
                            <p className="reg_label">
                               Nama     
                            </p>
                            <input type="text" id="userName" name="userName" />
                        </div>
                        <div className="reg_inp">
                            <p className="reg_label">
                               No Telephone   
                            </p>
                            <input type="text" id="phone" name="phone" />
                        </div>
                        <div className="reg_inp">
                            <p className="reg_label">
                               email    
                            </p>
                            <input type="email" id="email" name="email" />
                        </div>
                        <div className="reg_inp">
                            <p className="reg_label">
                               password    
                            </p>
                            <input type="password" id="password" name="password" />
                        </div>
                        <div className="reg_inp">
                            <p className="reg_label">
                               confirm password    
                            </p>
                            <input type="password" id="password" name="password" />
                        </div>
                        {/* <div className="reg_xyz">
                            <input type="checkbox" />
                            <p>kosong dulu</p>
                        </div> */}
                        <button type="button" id="submit" className="reg_btn">
                            Daftar
                        </button>
                    </div>
                </form>
            </div> 

        </div>
    )
}

export default register;